let firstNumber = +prompt("Input first number");
let secondNumber = +prompt("Input second number");
let mathOperation = prompt("Input one of math's operations: +, -, / or *");

function calcMathOperation (firstNumber, secondNumber, mathOperation) {

    switch (mathOperation) {
        case "+":
            let resultSum = firstNumber + secondNumber
            console.log(resultSum);
            break;
        case "-":
            let resultSubtraction = firstNumber - secondNumber
            console.log(resultSubtraction);
            break;
        case "*":
            let resultMultiplication = firstNumber * secondNumber
            console.log(resultMultiplication);
            break;
        case "/":
            let resultDivision = firstNumber / secondNumber
            console.log(resultDivision);
            break;
    }
}
calcMathOperation(firstNumber, secondNumber, mathOperation);
